#!/bin/bash

IF=$2

./run80.sh $IF $1
./run443.sh $IF $1
./runget80.sh $IF $1
./runget443.sh $IF $1
./runack80.sh $IF $1
./runack443.sh $IF $1
./runfin80.sh $IF $1
./runfin443.sh $IF $1
sleep 10m
pkill script80.sh
pkill script443.sh
pkill scriptget80.sh
pkill scriptget443.sh
pkill scriptack80.sh
pkill scriptack443.sh

apt-get purge sshpass automake libtool  libpcap-dev libjson0 libjson0-dev lua-ldoc libnetfilter-queue-dev autotools-dev autoconf gcc g++ make liblua5.1-0-dev liblua50-dev liblualib50-dev python-scapy


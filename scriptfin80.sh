#!/bin/bash


date="$(date -I)"

if [ -z "$1" ]
then
	test="target"
else
	test=$1
fi

round=0
address=0
dport=80
iface=$2
folder=$3
while read ip
do

part=$((address/500))
address=$((address+1))

python -u traceboxfin.py $ip $dport $iface >> $folder/res_${test}_fin80_${date}_part_${part}_port_${dport}_iface_${iface}

done < ${test}

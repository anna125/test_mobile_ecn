#!/bin/bash

date="$(date -I)"

if [ -z "$1" ]
then
	test="target"
else
	test=$1
fi

round=0
address=0
dport=443
iface=$2
folder=$3

while read ip
do

echo $dport
part=$((address/500))
address=$((address+1))

#test0 standard SYN
echo "test0;$ip;$dport" >> $folder/res_${test}_syn443_${date}_part_${part}_port_${dport}_iface_${iface}
tracebox -i $iface -p "ip({dscp=10})/tcp({dst=$dport})" $(awk '{print $1}' <<<"$ip") -m 25 -t 0.5 -v >> $folder/res_${test}_syn443_${date}_part_${part}_port_${dport}_iface_${iface}

#test1 standard ECN
echo "test1;$ip;$dport" >> $folder/res_${test}_syn443_${date}_part_${part}_port_${dport}_iface_${iface}
tracebox -i $iface -p "ip({ecn=0,dscp=12})/tcp({flags=194,dst=$dport})" $(awk '{print $1}' <<<"$ip") -m 25 -t 0.5 -v >> $folder/res_${test}_syn443_${date}_part_${part}_port_${dport}_iface_${iface}

#test2 ECT(0)
echo "test2;$ip;$dport" >> $folder/res_${test}_syn443_${date}_part_${part}_port_${dport}_iface_${iface}
tracebox -i $iface -p "ip({ecn=2,dscp=14})/tcp({dst=$dport})"  $(awk '{print $1}' <<<"$ip") -m 25 -t 0.5 -v >> $folder/res_${test}_syn443_${date}_part_${part}_port_${dport}_iface_${iface}

#test3 ECT(0) + ECN
echo "test3;$ip;$dport" >> $folder/res_${test}_syn443_${date}_part_${part}_port_${dport}_iface_${iface}
tracebox -i $iface -p "ip({ecn=2,dscp=18})/tcp({flags=194,dst=$dport})" $(awk '{print $1}' <<<"$ip") -m 25 -t 0.5 -v >> $folder/res_${test}_syn443_${date}_part_${part}_port_${dport}_iface_${iface}

#test4 ECT(1)
echo "test4;$ip;$dport" >> $folder/res_${test}_syn443_${date}_part_${part}_port_${dport}_iface_${iface}
tracebox -i $iface -p "ip({ecn=1,dscp=20})/tcp({dst=$dport})" $(awk '{print $1}' <<<"$ip") -m 25 -t 0.5 -v >> $folder/res_${test}_syn443_${date}_part_${part}_port_${dport}_iface_${iface}

#test5 ECT(1)  + ECN
echo "test5;$ip;$dport" >> $folder/res_${test}_syn443_${date}_part_${part}_port_${dport}_iface_${iface}
tracebox -i $iface -p "ip({ecn=1,dscp=22})/tcp({flags=194,dst=$dport})" $(awk '{print $1}' <<<"$ip") -m 25 -t 0.5 -v >> $folder/res_${test}_syn443_${date}_part_${part}_port_${dport}_iface_${iface}

#test6 CE
echo "test6;$ip;$dport" >> $folder/res_${test}_syn443_${date}_part_${part}_port_${dport}_iface_${iface}
tracebox -i $iface -p "ip({ecn=3,dscp=26})/tcp({dst=$dport})" $(awk '{print $1}' <<<"$ip") -m 25 -t 0.5 -v >> $folder/res_${test}_syn443_${date}_part_${part}_port_${dport}_iface_${iface}

#test7 CE  + ECN
echo "test7;$ip;$dport" >> $folder/res_${test}_syn443_${date}_part_${part}_port_${dport}_iface_${iface}
tracebox -i $iface -p "ip({ecn=3,dscp=28})/tcp({flags=194,dst=$dport})" $(awk '{print $1}' <<<"$ip") -m 25 -t 0.5 -v >> $folder/res_${test}_syn443_${date}_part_${part}_port_${dport}_iface_${iface}


done < ${test}
